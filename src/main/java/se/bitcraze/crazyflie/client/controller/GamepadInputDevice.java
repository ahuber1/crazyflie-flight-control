/* 
 *  Copyright (C) 2014 Andreas Huber
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package se.bitcraze.crazyflie.client.controller;

import java.util.Properties;

import net.java.games.input.Component;
import net.java.games.input.Controller;

/**
 * @author Andreas Huber
 * 
 */
public class GamepadInputDevice extends AbstractInputDevice {
	private Component yawAxis;
	private Component thrustAxis;
	private Component pitchAxis;
	private Component rollAxis;

	/**
	 * @param controller
	 * @param properties
	 */
	public GamepadInputDevice(Controller controller, Properties properties) {
		super(controller, properties);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflie.client.controller.AbstractInputDevice#initialize
	 * (net.java.games.input.Controller, java.util.Properties)
	 */
	@Override
	protected void initialize(Controller controller, Properties properties) {
		super.initialize(controller, properties);
		String roll = properties.getProperty("controller.gamepad.roll", "rz");
		String pitch = properties.getProperty("controller.gamepad.pitch",
				"slider");
		String yaw = properties.getProperty("controller.gamepad.yaw", "x");
		String thrust = properties
				.getProperty("controller.gamepad.thrust", "y");
		for (Component c : controller.getComponents()) {
			String id = c.getIdentifier().getName();
			if (roll.equals(id)) {
				rollAxis = c;
			} else if (pitch.equals(id)) {
				pitchAxis = c;
			} else if (yaw.equals(id)) {
				yawAxis = c;
			} else if (thrust.equals(id)) {
				thrustAxis = c;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * se.bitcraze.crazyflie.client.controller.AbstractInputDevice#pollData()
	 */
	@Override
	protected void pollData() {
		super.pollData();

		notifyEvent(new AxisControlEvent(yawAxis.getPollData(),
				thrustAxis.getPollData(), pitchAxis.getPollData(),
				rollAxis.getPollData()));

	}
}
